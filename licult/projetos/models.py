from django.db import models
from django.utils.translation import gettext as _

class Projeto(models.Model):
    nome = models.CharField(_('Nome do Projeto'), max_length=350, blank=False)
    n_processo = models.IntegerField(_('Numero do Projeto'), max_length=30)
    proponente = models.CharField(_('Nome do Proponente'), max_length=150, blank=False)
    email = models.EmailField(_('Email do Proponente'))
    telefone = models.IntegerField(_('Telefone'), max_length=12)
    segmento = models.CharField(_('Segmento Cultural'), max_length=130)
    identificacao = models.TextField(_('Identificação'), help_text=_('Objeto + Justificativa'))
    local_realizacao = models.CharField(_('Local de realização'), max_length=100)
    periodo = models.DateField(_('Periodo de Execução'))
    publico_selecionado = models.TextField(_('Publico Selecionado'))
    entrada_gratuita = models.BooleanField(_('Entrada Gratuita'))
    valor_total = models.FloatField(_('Valor total do Projeto'))
    valor_lic = models.FloatField(_('Valor Solicitado via LIC'))
    aprovado = models.BooleanField(_('Aprovado'))
    carta_captacao = models.BooleanField(_('Possui Carta de Captação?'))
    adicionado_em = models.DateField(_('Adicionado em:'), auto_now_add=True)

    class Meta:
        verbose_name='projeto'
        verbose_name_plural='projetos'

